<!doctype html>
<html lang="en">

<head>
  <title>Edit Password</title>
  <link rel="stylesheet" href="./css/style.css">
</head>

<body onload="getUserDetailsOnEdit(); currentTime()">
  <nav class="nav">
  <a href="./dashboard.php"><h3 class="nav-heading">Dashboard</h3></a>
    <ul class="nav-item-left">
      <li class="nav-item">
        <a class="nav-link" aria-current="page" href="./dashboard.php">TO-DO</a>
      </li>
      <li class="nav-item">
        <a id='linkProfile' class="nav-link" aria-current="page" href="./profile.php">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="./edit-profile.php">Edit Profile</a>
      </li>
      <li class="nav-item">
        <a id='linkTodo' class="nav-link" aria-current="page" href="./change-password.php">Change Password</a>
      </li>
    </ul>
    <div class="nav-item-right">
      <p id="currentTime" class=""></p>
      <input type="button" onclick="return confirm('Are you sure you want to logout?')?logout():''" class="btn btn-lightblue justify-self-right" value="Logout">
    </div>
  </nav>

  <div class="dashboard-container">
    <div class="card">
      <div class="card-heading">
        <h3>Edit Profile</h3>
      </div>
      <label for="name" class="form-label">Old Password</label>
      <input id="name" type="text" value='' class="form-control mb-1" placeholder="Name">
      <label for="email" class="form-label">New Password</label>
      <input id="email" value='' type="email" size="30" class="form-control mb-1" placeholder="Email">
      <label for="mobile" class="form-label">New Password</label>
      <input id="mobile" type="text" value='' class="form-control mb-1" placeholder="Mobile">
      <div class="d-flex">
      <input type="button" onclick="editProfile()" value="Submit" class="btn btn-blue"><br>
      </div>
    </div>
  </div>
  <script src="./js/user.js"></script>
</body>

</html>