<!DOCTYPE html>
<html lang="en">

<head>
	<title>Dashboard</title>
	<link rel="stylesheet" href="./css/style.css">
</head>

<body onload="currentTime(); getTodos(); getWeather(); getGifs();">

	<nav class="nav">
		<a href="./dashboard.php">
			<h3 class="nav-heading">Dashboard</h3>
		</a>
		<ul class="nav-item-left">
			<li class="nav-item">
				<a class="nav-link active" aria-current="page" href="./dashboard.php">TO-DO</a>
			</li>
			<li class="nav-item">
				<a id='linkProfile' class="nav-link" aria-current="page" href="./profile.php">Profile</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" aria-current="page" href="./edit-profile.php">Edit Profile</a>
			</li>
			<li class="nav-item">
				<a id='linkTodo' class="nav-link" aria-current="page" href="./change-password.php">Change Password</a>
			</li>
		</ul>
		<div class="nav-item-right">
			<p id="currentTime" class=""></p>
			<input type="button" onclick="return confirm('Are you sure you want to logout?')?logout():''" class="btn btn-lightblue justify-self-right" value="Logout">
		</div>
	</nav>

	<div class="todo-container">
		<div class="sidebar">
			<div class="side-card">
				<h2 id="temperature"></h2>
				<h2 id="description"></h2>
				<p id="location"></p>
			</div>
			<img id="gifs" class="side-gif" src="" alt="" srcset="">
		</div>

		<div class="todo-card">
			<div class="card-heading">
				<h2>To-do List</h2>
			</div>
			<div class="row">
				<input id="txtTodo" type="text" class="form-control w-75" placeholder="Add to-do" />
				<input id="btnTodo" type="button" onclick="addTodo()" class="btn btn-blue w-25 m-9" value="Add" />
				<!-- <div class="col-7">
				</div>
				<div class="col-7">
				</div> -->
			</div>
			<div class="row">
				<div id="todos" class=""></div>
				<div id="completedtodo" class="ml-10" class=""></div>
			</div>
		</div>

	</div>
</body>
<script src="./js/user.js"></script>

</html>