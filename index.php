<!doctype html>
<html lang="en">

<head>
  <title>Login</title>
  <link href="./css/style.css" rel="stylesheet">
</head>

<body>

  <div class="container">
    <div class="card">
      <div class="card-heading">
        <h3>Login</h3>
      </div>
      <label for="email" class="form-label">Email</label>
      <input id="email" type="email" class="form-control" placeholder="Email">
      <label for="email" class="form-label">Password</label>
      <input id="password" type="password" class="form-control" placeholder="Password">
      <div class="d-flex">
        <input type="button" onclick="loginValidate()" value="Login" class="btn btn-blue"><br>
      </div>
      <div class="d-flex flex-row justify-content-center">
        <a href="./register.php" class="link">New user? Register here</a>
      </div>
    </div>
  </div>
  <script src="./js/script.js"></script>
</body>

</html>