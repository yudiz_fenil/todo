const mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const mobileformat = /^([6-9]{1})([0-9]{1})([0-9]{8})$/
const aUser = []

// User Registration

function registerValidate() {
    const oUser = {}
	const userArray = JSON.parse(localStorage.getItem("users"));
    const name = document.getElementById('name').value.trim();
    const email = document.getElementById('email').value.trim().toLowerCase();
    const password = document.getElementById('password').value;
    const mobile = document.getElementById('mobile').value.trim();
    const dob = document.getElementById('dob').value;
    const male = document.getElementById('male');
    const female = document.getElementById('female');
    let gender;
    if(male.checked || female.checked){
        gender = (()=> male.checked?'Male':'Female')()
    }

    if(name=='' || email=='' || password=='' || mobile=='' || dob=='' || gender==undefined){
        alert('Please fill all details')
        return false;
    }

    if(password.length<8 || password.length>16){
        alert('Your password must be 8-16 characters long')
        return false;
    }

    if(!mobile.match(mobileformat)){
        alert('Invalid mobile number')
        return false;
    }

    if(!email.match(mailformat)){
        alert('Enter valid email')
        return false;
    }
	if(userArray!=null){		
		for(let i=0;i<userArray.length;i++){
			if(email===userArray[i].email){
				alert('User already exists')
				return false;
			}
		}
	}

    oUser.name = name;
    oUser.email = email;
    oUser.password = password;
    oUser.mobile = mobile;
    oUser.dob = dob;
    oUser.gender = gender;
    oUser.todos = [];
    oUser.ctodos = [];
	if(userArray==null){		
		aUser.push(oUser)
		localStorage.setItem("users", JSON.stringify(aUser));
	}else{
		userArray.push(oUser);
		localStorage.setItem("users", JSON.stringify(userArray));
	}
	console.log(aUser)
	window.location = 'index.php';
}

// User Login

function loginValidate(){
	
	const users = JSON.parse(localStorage.getItem("users"));
	if(users==null){
		alert('Invalid Credentials')
		return false;
	}
	
    const email = document.getElementById('email').value.trim().toLowerCase();
    const password = document.getElementById('password').value;
    let isUser = false;

    for(let i=0;i<users.length;i++){
        if(email==users[i].email && password==users[i].password){
            sessionStorage.setItem('userIndex',i)
			sessionStorage.setItem('email',email)
			isUser=true;
			break;
        }
    }
    if(isUser){
        window.location = "dashboard.php";
    }else{
        alert('Invalid Credentials')
    }
}
