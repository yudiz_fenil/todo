const sessionEmail = sessionStorage.getItem("email");
if (sessionEmail === null) {
	window.location = "index.php";
}
const mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const mobileformat = /^([6-9]{1})([234789]{1})([0-9]{8})$/
const userIndex = sessionStorage.getItem("userIndex");
let gifIndex = 0;

let aUser = JSON.parse(localStorage.getItem('users'));
let oUser = {};
oUser = aUser[userIndex]
let editTodoIndex = 0;

// Logout

function logout() {
	sessionStorage.clear();
	window.location = "index.php";
}

// Profile

function getUserDetails() {
	let name = document.getElementById("name").innerHTML = oUser.name;
	let email = document.getElementById("email").innerHTML = oUser.email;
	let mobile = document.getElementById("mobile").innerHTML = oUser.mobile;
	let dob = document.getElementById("dob").innerHTML = oUser.dob;
	let gender = document.getElementById("gender").innerHTML = oUser.gender;
}

// Get user details on edit page

function getUserDetailsOnEdit() {
	document.getElementById("name").value = oUser.name;
	document.getElementById("email").value = oUser.email;
	document.getElementById("mobile").value = oUser.mobile;
}

// Getting current time

function currentTime() {
	getTime()
	function getTime() {
		const d = new Date();
		document.getElementById('currentTime').innerText = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
	}
	setInterval(getTime, 1000)
}

setInterval(currentTime, 10000)

// Change password

function changePassword() {
	let oldPassword = document.getElementById('oldPassword').value;
	let newPassword = document.getElementById('newPassword').value;
	const password = oUser.password;
	if (oldPassword == '' || newPassword == '') {
		alert('Please enter passwords')
		return false;
	}
	if (newPassword.length < 8 || newPassword.length > 16) {
		alert('Your new password must be 8-16 characters long')
		return false;
	}
	if (password === oldPassword) {
		aUser[userIndex].password = newPassword;
		localStorage.setItem("users", JSON.stringify(aUser));
		alert('Password updated successfully')
		document.getElementById('oldPassword').value = "";
		document.getElementById('newPassword').value = "";
	} else {
		alert('Old password is incorrect')
		console.log(newPassword)
		return false;
	}
}

// Edit profile

function editProfile() {
	let name = document.getElementById('name').value.trim();
	let email = document.getElementById('email').value.trim().toLowerCase();
	let mobile = document.getElementById('mobile').value.trim();

	if (name == '' || email == '' || mobile == '') {
		alert('Please enter details')
		return false;
	}
	if (!mobile.match(mobileformat)) {
		alert('Invalid mobile number')
		return false;
	}
	if (!email.match(mailformat)) {
		alert('Enter valid email')
		return false;
	}
	if (confirm('Are you sure you want to make changes?')) {
		aUser[userIndex].name = name;
		aUser[userIndex].email = email;
		aUser[userIndex].mobile = mobile;
		localStorage.setItem("users", JSON.stringify(aUser));
		document.getElementById('name').value = "";
		document.getElementById('email').value = "";
		document.getElementById('mobile').value = "";
		alert('Profile updated successfully')
	}
}

// TODO

function getTodos() {
	const todoArray = aUser[userIndex].todos;
	const cTodoArray = aUser[userIndex].ctodos;
	const todos = document.getElementById('todos');
	const completedtodo = document.getElementById('completedtodo');
	todos.classList.remove('todos')
	completedtodo.classList.remove('completedtodo')
	todos.innerHTML = "";
	completedtodo.innerHTML = "";

	if (aUser[userIndex].todos.length == 0 && aUser[userIndex].ctodos.length == 0) {
		const heading = document.createElement('p');
		heading.innerHTML = "No to-do for the day...";
		heading.classList.add('todo-heading');
		todos.appendChild(heading);
		return false;
	}

	// Pending todos

	if (aUser[userIndex].todos.length != 0) {
		todos.classList.add('todos')
		let heading = document.createElement('p');
		heading.innerHTML = "List of pending to-do";
		heading.classList.add('todo-heading');
		todos.appendChild(heading);
		for (let i = 0; i < aUser[userIndex].todos.length; i++) {
			const todo = document.createElement('p');
			todo.innerHTML = `${i + 1}. ${todoArray[i]}`;
			todo.classList.add('todo-text');
			todos.appendChild(todo);

			const editTodo = document.createElement('input');
			editTodo.id = 'editTodo' + i;
			editTodo.type = 'button';
			editTodo.value = 'Edit';
			editTodo.classList.add('ml-4');
			editTodo.classList.add('btn-outline-blue');
			editTodo.setAttribute('onclick', `editTodo(${i})`)
			todos.appendChild(editTodo);

			const deleteTodo = document.createElement('input');
			deleteTodo.id = 'deleteTodo' + i;
			deleteTodo.type = 'button';
			deleteTodo.value = 'Delete';
			deleteTodo.classList.add('ml-4');
			deleteTodo.classList.add('btn-outline-crimson');
			deleteTodo.setAttribute('onclick', `removeTodo(${i})`)
			todos.appendChild(deleteTodo);

			const statusTodo = document.createElement('input');
			statusTodo.id = 'statusTodo' + i;
			statusTodo.type = 'checkbox';
			statusTodo.classList.add('todocheck');
			statusTodo.classList.add('ml-10');
			statusTodo.setAttribute('onclick', `statusTodo(${i})`)
			todos.appendChild(statusTodo);
		}
	}

	// Completed todos

	if (aUser[userIndex].ctodos.length != 0) {
		completedtodo.classList.add('completedtodo')
		let heading = document.createElement('p');
		heading.innerHTML = "List of completed to-do";
		heading.classList.add('todo-heading');
		completedtodo.appendChild(heading);

		for (let i = 0; i < aUser[userIndex].ctodos.length; i++) {
			const ctodo = document.createElement('p');
			ctodo.innerText = `${i + 1}. ${cTodoArray[i]}`;
			ctodo.classList.add('todo-text');
			completedtodo.appendChild(ctodo);

			const deleteTodo = document.createElement('input');
			deleteTodo.id = 'deleteTodo' + i;
			deleteTodo.type = 'button';
			deleteTodo.value = 'Delete';
			deleteTodo.classList.add('ml-4');
			deleteTodo.classList.add('btn-outline-crimson');
			deleteTodo.setAttribute('onclick', `removeCompletedTodo(${i})`)
			completedtodo.appendChild(deleteTodo);

			const statusTodo = document.createElement('input');
			statusTodo.id = 'statusTodo' + i;
			statusTodo.type = 'checkbox';
			statusTodo.classList.add('todocheck');
			statusTodo.classList.add('ml-10');
			statusTodo.setAttribute('onclick', `statusTodo(${i})`)
			statusTodo.setAttribute('checked', '')
			completedtodo.appendChild(statusTodo);
		}
	}
}

function addTodo() {
	const txtTodo = document.getElementById('txtTodo').value;
	if (txtTodo == "") {
		alert('Add to-do')
		return false;
	}
	if (document.getElementById('btnTodo').value != "Update") {
		aUser[userIndex].todos.push(txtTodo)
	} else {
		aUser[userIndex].todos.splice(editTodoIndex, 1, txtTodo);
		document.getElementById('btnTodo').value = "Add";
	}
	localStorage.setItem("users", JSON.stringify(aUser));
	getTodos()
	document.getElementById('txtTodo').value = "";
}

function editTodo(index) {
	document.getElementById('btnTodo').value = "Update";
	document.getElementById('txtTodo').value = aUser[userIndex].todos[index];
	editTodoIndex = index;
}
function removeTodo(index) {
	aUser[userIndex].todos.splice(index, 1);
	localStorage.setItem("users", JSON.stringify(aUser));
	getTodos()
}
function removeCompletedTodo(index) {
	aUser[userIndex].ctodos.splice(index, 1);
	localStorage.setItem("users", JSON.stringify(aUser));
	getTodos()
}
function statusTodo(index) {
	const checkbtn = document.getElementById(`statusTodo${index}`);
	console.log(checkbtn.checked);
	if (checkbtn.checked) {
		aUser[userIndex].ctodos.push(aUser[userIndex].todos[index])
		aUser[userIndex].todos.splice(index, 1);
		console.log('checked')
	} else {
		aUser[userIndex].todos.push(aUser[userIndex].ctodos[index])
		aUser[userIndex].ctodos.splice(index, 1);
		console.log('not checked')
	}
	localStorage.setItem("users", JSON.stringify(aUser));
	getTodos()
}

// Weather

function getWeather() {
	const temperature = document.getElementById("temperature");
	const description = document.getElementById("description");
	const location = document.getElementById("location");

	const api = "https://api.openweathermap.org/data/2.5/weather";
	const apiKey = "a4f34114df09ff8466f17741eea80936";
	location.innerHTML = "Locating...";
	navigator.geolocation.getCurrentPosition(success, error);
	async function success(position) {
		latitude = position.coords.latitude;
		longitude = position.coords.longitude;
		const url = api + "?lat=" + latitude + "&lon=" + longitude + "&appid=" + apiKey + "&units=imperial";
		const response = await fetch(url);
		const json = await response.json();
		temperature.innerHTML = Math.round(json.main.temp) + "° F";
		location.innerHTML = json.name;
		description.innerHTML = json.weather[0].main;
	}
	function error() {
		location.innerHTML = "Unable to retrieve your location";
	}
}

// Random Gifs

async function getGifs() {
	const gifUrl = 'https://api.giphy.com/v1/gifs/search?q=spider+man&api_key=Poco4yPP44g5bmUoNJ4HVPkDnARgQI0e';
	const gifResponse = await fetch(gifUrl);
	const gifJSON = await gifResponse.json();
	document.getElementById('gifs').src = gifJSON.data[gifIndex].images.original.url;
	gifIndex++;
}
setInterval(getGifs, 2*60*1000)