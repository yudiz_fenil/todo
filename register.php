<!doctype html>
<html lang="en">

<head>
    <title>Register</title>
    <link href="./css/style.css" rel="stylesheet">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
</head>

<body>
    <div class="container">
        <div class="card">
            <div class="card-heading">
                <h3>Registration</h3>
            </div>
            <label for="name" class="form-label">Name</label>
            <input id="name" type="text" class="form-control m-2" placeholder="Name" autofocus>
            <label for="email" class="form-label">Email</label>
            <input id="email" type="email" size="30" class="form-control m-2" placeholder="Email">
            <label for="password" class="form-label">Password</label>
            <input id="password" type="password" class="form-control m-2 mb-0" placeholder="Password">
            <div id="passwordHelpBlock" class="form-text">Your password must be 8-16 characters long.</div>
            <label for="mobile" class="form-label">Mobile</label>
            <input id="mobile" type="text" class="form-control m-2 mb-0" placeholder="Mobile">
            <div id="passwordHelpBlock" class="form-text">10 digits mobile number.</div>
            <label for="dob" class="form-label">Date of Birth</label>
            <input id="dob" type="date" class="form-control m-2">
            <label for="gender" class="form-label">Gender</label>
            <div class="d-flex">
                <input id="male" type="radio" name="gender" class="form-radio" value="Male">Male
                <input id="female" type="radio" name="gender" class="form-radio" value="Female">Female
            </div>
            <div class="d-flex">
                <input type="button" onclick="registerValidate()" value="Register" class="btn btn-blue">
            </div>
            <div class="d-flex">
                <a href="./index.php" class="link">Alredy user? Login here</a>
            </div>
        </div>
    </div>
    <script src="./js/script.js"></script>
</body>

</html>