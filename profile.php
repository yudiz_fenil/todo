<!DOCTYPE html>
<html lang="en">

<head>
    <title>Profile</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body onload="getUserDetails(); currentTime()">
    <nav class="nav">
        <a href="./dashboard.php">
            <h3 class="nav-heading">Dashboard</h3>
        </a>
        <ul class="nav-item-left">
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="./dashboard.php">TO-DO</a>
            </li>
            <li class="nav-item">
                <a id='linkProfile' class="nav-link active" aria-current="page" href="./profile.php">Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="./edit-profile.php">Edit Profile</a>
            </li>
            <li class="nav-item">
                <a id='linkTodo' class="nav-link" aria-current="page" href="./change-password.php">Change Password</a>
            </li>
        </ul>
        <div class="nav-item-right">
            <p id="currentTime" class=""></p>
            <input type="button" onclick="return confirm('Are you sure you want to logout?')?logout():''" class="btn btn-lightblue justify-self-right" value="Logout">
        </div>
    </nav>


    <div class="dashboard-container">
        <div class="card">
            <div class="card-heading">
                <h3>User Profile</h3>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="name" class="form-label">Name</label>
                </div>
                <div class="col-7">
                    <label id="name" class="form-label"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="email" class="form-label">Email</label>
                </div>
                <div class="col-7">
                    <label id="email" class="form-label"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="mobile" class="form-label">Mobile</label>
                </div>
                <div class="col-7">
                    <label id="mobile" class="form-label"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="dob" class="form-label">Date of Birth</label>
                </div>
                <div class="col-7">
                    <label id="dob" class="form-label"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="gender" class="form-label">Gender</label>
                </div>
                <div class="col-7">
                    <label id="gender" class="form-label"></label>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="./js/user.js"></script>

</html>