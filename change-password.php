<!doctype html>
<html lang="en">

<head>
  <title>Change Password</title>
  <link rel="stylesheet" href="./css/style.css">
</head>

<body onload="currentTime()">
  <nav class="nav">
    <a href="./dashboard.php">
      <h3 class="nav-heading">Dashboard</h3>
    </a>
    <ul class="nav-item-left">
      <li class="nav-item">
        <a class="nav-link" aria-current="page" href="./dashboard.php">TO-DO</a>
      </li>
      <li class="nav-item">
        <a id='linkProfile' class="nav-link" aria-current="page" href="./profile.php">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" aria-current="page" href="./edit-profile.php">Edit Profile</a>
      </li>
      <li class="nav-item">
        <a id='linkTodo' class="nav-link active" aria-current="page" href="./change-password.php">Change Password</a>
      </li>
    </ul>
    <div class="nav-item-right">
      <p id="currentTime" class=""></p>
      <input type="button" onclick="return confirm('Are you sure you want to logout?')?logout():''" class="btn btn-lightblue justify-self-right" value="Logout">
    </div>
  </nav>
  <div class="dashboard-container">
    <div class="card">
      <div class="card-heading">
        <h3>Change Password</h3>
      </div>
      <label for="oldpassword" class="form-label">Old Password</label>
      <input id="oldPassword" type="password" class="form-control" placeholder="Old Password">
      <label for="newpassword" class="form-label">New Password</label>
      <input id="newPassword" type="password" class="form-control" placeholder="New Password">
      <div id="passwordHelpBlock" class="form-text">Your password must be 8-16 characters long.</div>
      <div class="d-flex">
        <input type="button" onclick="changePassword()" value="Submit" class="btn btn-blue"><br>
      </div>
    </div>
  </div>
  <script src="./js/user.js"></script>
</body>

</html>